angular.module('starter.controllers', ['ionic', 'starter.services','ionic.contrib.ui.tinderCards'])

    .controller('SplashCtrl', function($scope, $state, User) {
	/*
	$scope.submitForm = function(username, signingUp, password) {
	    User.auth(username,signingUp, password).then(function() {
		$state.go('tab.question');
	    });
	}
	*/
	$scope.submitForm = function() {
	    $state.go('tab.question');
	}
    })

    .controller('QuestionCtrl', function($scope, $ionicTabsDelegate, $ionicGesture) {
	$scope.goForward = function() {
	    var selected = $ionicTabsDelegate.selectedIndex();
	    if(selected != -1) {
		$ionicTabsDelegate.select(selected + 1);
	    }
	}
	$scope.goDown = function(event) {
	    alert('q is the best');
	}
    })

    .controller('myQuestionCtrl', function($scope, $ionicTabsDelegate) {
	$scope.active = 'MyQuestion';
	$scope.setActive = function(type) {
	    $scope.active = type;
	}
	$scope.isActive = function(type) {
	    return type === $scope.active;
	}

	$scope.goForward = function() {
	    if($scope.active == 'MyQuestion') {
		$scope.active = 'favorites';
	    }
	    else  {
		var selected = $ionicTabsDelegate.selectedIndex();
		if(selected != -1) {
		    $ionicTabsDelegate.select(selected + 1);
		}
	    }
	}


	$scope.goBack = function() {
	    if($scope.active == 'favorites') {
		$scope.active = 'MyQuestion';
	    }
	    else {
		var selected = $ionicTabsDelegate.selectedIndex();
		if(selected != 0 && selected != -1) {
		    $ionicTabsDelegate.select(selected - 1);
		}
	    }
	}
	
    })

    .controller('FavoritesCtrl', function($scope, $ionicTabsDelegate,  $ionicSlideBoxDelegate) {
	/*
	$scope.goBack = function() {
	    var selected = $ionicTabsDelegate.selectedIndex();
	    if(selected != 0 && selected != -1) {
		$ionicTabsDelegate.select(selected - 1);
	    }
	}
*/
	$scope.navSlide = function(index) {
	    $ionicSlideBoxDelegate.slide(index, 500);
	}
    })
   


    .controller('CardsCtrl', function($scope) {
	var cardTypes = [
            { image: 'img/pic2.png', title: 'So much grass #hippster'},
            { image: 'img/pic3.png', title: 'Way too much Sand, right?'},
            { image: 'img/pic4.png', title: 'Beautiful sky from wherever'},
	];

	$scope.cards = [];
	
	$scope.addCard = function(i) {
            var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
            newCard.id = Math.random();
            $scope.cards.push(angular.extend({}, newCard));
	}
	
	for(var i = 0; i < 3; i++) $scope.addCard();
	
	$scope.cardSwipedLeft = function(index) {
            console.log('Left swipe');
	}
	
	$scope.cardSwipedRight = function(index) {
            console.log('Right swipe');
	}
	
	$scope.cardDestroyed = function(index) {
            $scope.cards.splice(index, 1);
            console.log('Card removed');
	}
    })

    .controller('TabsCtrl', function($scope,$ionicPlatform) {
	$ionicPlatform.onHardwareBackButton(function() {
	    navigator.app.exitApp();
	})
    });
