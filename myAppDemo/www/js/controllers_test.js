angular.module('starter.controllers', ['ionic', 'starter.services','ionic.contrib.ui.cards'])

    .controller('SplashCtrl', function($scope, $state, User) {
	/*
	$scope.submitForm = function(username, signingUp, password) {
	    User.auth(username,signingUp, password).then(function() {
		$state.go('tab.question');
	    });
	}
	*/
	$scope.submitForm = function() {
	    $state.go('tab.question');
	}
    })

    .controller('QuestionCtrl', function($scope) {

    })

    .controller('myQuestionCtrl', function($scope) {
	$scope.active = 'MyQuestion';
	$scope.setActive = function(type) {
	    $scope.active = type;
	}
	$scope.isActive = function(type) {
	    return type === $scope.active;
	}
	
    })

    .controller('FavoritesCtrl', function($scope) {

    })

    .controller('CardsCtrl', function($scope, $ionicSwipeCardDelegate) {
	var cardTypes = [{
	    title: 'Swipe down to clear the card',
	    image: 'img/pic.png'
	}, {
	    title: 'Where is this?',
	    image: 'img/pic.png'
	}, {
	    title: 'What kind of grass is this?',
	    image: 'img/pic2.png'
	}, {
	    title: 'What beach is this?',
	    image: 'img/pic3.png'
	}, {
	    title: 'What kind of clouds are these?',
	    image: 'img/pic4.png'
	}];
	
	$scope.cards = Array.prototype.slice.call(cardTypes, 0, 0);
	
	$scope.cardSwiped = function(index) {
	    $scope.addCard();
	};
	
	$scope.cardDestroyed = function(index) {
	    $scope.cards.splice(index, 1);
	};
	
	$scope.addCard = function() {
	    var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
	    newCard.id = Math.random();
	    $scope.cards.push(angular.extend({}, newCard));
	}
    })

    .controller('CardCtrl', function($scope, $ionicSwipeCardDelegate) {
	$scope.goAway = function() {
	    var card = $ionicSwipeCardDelegate.getSwipeableCard($scope);
	    card.swipe();
	};
    })

    .controller('TabsCtrl', function($scope,$ionicPlatform) {
	$ionicPlatform.onHardwareBackButton(function() {
	    navigator.app.exitApp();
	})
	
    })

    .directive('tabsSwipable', ['$ionicGesture', function($ionicGesture) {
	return {
	    restrict: 'A',
	    require: 'ionTabs',
	    link: function(scope, elm, attrs, tabsCtrl) {
		var onSwipeLeft = function() {
		    var target = tabsCtrl.selectedIndex() + 1;
		    if( target < tabsCtrl.tabs.length) {
			scope.$apply(tabsCtrl.select(target));
		    }
		};
		var onSwipeRight = function() {
		    var target = tabsCtrl.selectedIndex() - 1;
		    if ( target >= 0) {
			scope.$apply(tabsCtrl.select(target));
		    }
		};

		var swipeGesture = $ionicGesture.on('swipeleft', onSwipeLeft, elm).on('swiperight', onSwipeRight);
		scope.$on('destroy', function() {
		    $ionicGesture.off(swipeGesture, 'swipeleft', onSwipeLeft);
		    $ionicGesture.off(swipeGusture, 'swiperight', onSwipeRight);
		});
	    }
	};
    }]);

