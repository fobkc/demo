// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers'])

    .run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
	    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	    // for form inputs)
	    if(window.cordova && window.cordova.plugins.Keyboard) {
		cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	    }
	    if(window.StatusBar) {
		StatusBar.styleDefault();
	    }
	})
	/*
	$ionicPlatform.onHardwareBackButton(function() {
	    AlertService.Confirm('System warning', 'Are you sure you want to exit?', function() {
		navigator.app.exitApp();
	    },
				 function() {
				     return;
				 })
	})
	*/
    })

    .config(function($ionicConfigProvider) {
	$ionicConfigProvider.tabs.position('top');
    })

    .config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
	

	
	// Set up an abstract state for the tabs directive:
	    .state('tab', {
		url: '/tab',
		abstract: true,
		templateUrl: 'templates/tabs.html',
		controller: 'TabsCtrl'
	    })
	
	// Each tab has its own nav history stack:
	
	    .state('tab.question', {
		url: '/question',
		views: {
		    'tab-question': {
			templateUrl: 'templates/question.html',
			controller: 'QuestionCtrl'
		    }
		}
	    })

	    .state('tab.myQuestion', {
		url: '/myQuestion',
		views: {
		    'tab-myQuestion': {
			templateUrl: 'templates/myQuestion.html',
			controller: 'myQuestionCtrl'
		    }
		}
	    })

	    .state('tab.favorites', {
		url: '/favorites',
		views: {
		    'tab-favorites': {
			templateUrl: 'templates/favorite.html',
			controller: 'FavoritesCtrl'
		    }
		}
	    })

	    .state('splash', {
		url:'/',
		templateUrl: 'templates/splash.html',
		controller: 'SplashCtrl'
	    })

	
	$urlRouterProvider.otherwise('/');
	
    })

    .constant('SERVER', {
	url: 'http://localhost:8000'
	//url: 'https://website?
    });
